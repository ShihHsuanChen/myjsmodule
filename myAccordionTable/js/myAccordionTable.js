function myAccordionTableActivate (jObj) {
    jObj.find('> tbody > .myaccordion_table-content').hide();
    jObj.on('click', '> tbody > .myaccordion_table-struct',  function() {
        $t = $(this);
        $c = $t.next();

        if($c.css("display")=="none"){
            console.log($c.parent())
            $c.slideDown();
            $t.find('.myaccordion_table-title').addClass("open");
        } else {
            $c.hide();
            $t.find('.myaccordion_table-title').removeClass("open");
        }
    }).mouseover(function(){
        $(this).addClass("rollover")
    }).mouseout(function(){
        $(this).removeClass("rollover")
    });
}

function myAccordionTableUnit (object_data, stop_layer=null) {
    var output = "";
    output += "<table class=myaccordion_table>";
    for (var key in object_data) {
        var content = object_data[key];

        var value = "";
        var struct = false;
        if ((content instanceof Object) && (stop_layer != 0)) {
            var N = Object.keys(content).length;
            value = "( " + N + " item(s) )";
            struct = true;
        } else if ((content instanceof Array) && (stop_layer != 0)) {
            var N = content.length;
            value = "( " + N + " item(s) )";
            struct = true;
        } else {
            value = content;
        }

        if (struct)
            output += "<tr class=myaccordion_table-struct>";
        else
            output += "<tr class=myaccordion_table-text>";
        output += "    <td class=myaccordion_table-title>" + key + " : </td>";
        output += "    <td class=myaccordion_table-value>" + value + "</td>";
        output += "</tr>";

        if (struct) {
            output += "<tr class=myaccordion_table-content>";
            output += "    <td colspan=2>";
            output += myAccordionTableUnit(
                content,
                (stop_layer == null) ? null : (stop_layer-1)
            )
            output += "    </td>";
            output += "</tr>";
        }
    }
    output += "</table>"
    return output;
}
