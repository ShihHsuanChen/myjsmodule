
class myTable {
    constructor (jParent, setting) {
        this.style_cls = 'mytable';
        var sel = '.' + this.style_cls;

        this.jObj = this._create(jParent);
        this._initialize(setting);

        this.bindEvents();
    }

    bindEvents() {
        this.jObj.off('click', '.click-enable');
        this.jObj.on('click', '.click-enable',
            this._operate_on_myfooter.click.bind(this)
        );
        this.jObj.on('input', '.global_search input',
            this._operate_on_pannel.search_input.bind(this)
        );
        this.jObj.off('click', '.global_search .css-close')
        this.jObj.on('click', '.global_search .css-close',
            this._operate_on_pannel.search_clear.bind(this)
        );
        this.jObj.off('click', '.batch_operation .back_button')
        this.jObj.on('click', '.batch_operation .back_button',
            this._operate_on_pannel.batch_operation_cancel.bind(this)
        );
        this.jObj.off('click', '.manual_button')
        this.jObj.on('click', '.manual_button',
            this._operate_on_pannel.click_manual_button.bind(this)
        );
        this.jObj.off('click', 'tbody tr');
        this.jObj.on('click', 'tbody tr',
            this._operate_on_tbody.click_row.bind(this)
        );
        this.jObj.off('click', 'tbody .checkbox_unchecked')
        this.jObj.on('click', 'tbody .checkbox_unchecked',
            this._operate_on_tbody.click_to_check.bind(this)
        );
        this.jObj.off('click', 'tbody .checkbox_checked')
        this.jObj.on('click', 'tbody .checkbox_checked',
            this._operate_on_tbody.click_to_uncheck.bind(this)
        );
        this.jObj.off('click', 'thead .sort_icon_none')
        this.jObj.on('click', 'thead .sort_icon_none',
            this._operate_on_thead.sort_icon_none.bind(this)
        );
        this.jObj.off('click', 'thead .sort_icon_asc')
        this.jObj.on('click', 'thead .sort_icon_asc',
            this._operate_on_thead.sort_icon_asc.bind(this)
        );
        this.jObj.off('click', 'thead .sort_icon_desc')
        this.jObj.on('click', 'thead .sort_icon_desc',
            this._operate_on_thead.sort_icon_desc.bind(this)
        );
        this.jObj.off('click', 'thead .checkbox_unchecked')
        this.jObj.on('click', 'thead .checkbox_unchecked',
            this._operate_on_thead.click_to_check.bind(this)
        );
        this.jObj.off('click', 'thead .checkbox_checked')
        this.jObj.on('click', 'thead .checkbox_checked',
            this._operate_on_thead.click_to_uncheck.bind(this)
        );
    }

    unbindEvents() {
        this.jObj.off();
        this.jObj.unbind();
    }

    destroy () {
    }

    _operate_on_myfooter = {
        click: function (e) {
            var tar = $(e.currentTarget);
            var tag = tar.attr('tag');
            var to_page = this._current_page;

            if (tag == 'first')
                this.firstPage();
            else if (tag == 'prev')
                this.prevPage();
            else if (tag == 'next')
                this.nextPage();
            else if (tag == 'last')
                this.lastPage();
            else if (tag == 'page')
                this.toPage(parseInt(tar.text()));
        }
    }

    _operate_on_pannel = {
        search_input: function (e) {
            var tar = $(e.currentTarget);
            var text = tar.val();
            this.filter(text);
        }, 

        search_clear: function (e) {
            var input_obj = this.jObj.find('.global_search input');
            input_obj.val(input_obj.attr('title'));
            input_obj.addClass('blur');
            this.filter(null);
        },

        batch_operation_cancel: function (e) {
            this._operate_on_thead.click_to_uncheck.bind(this)();
        },

        click_manual_button: function (e) {
            var tar = $(e.currentTarget);
            var uid = tar.attr('uid');
            var func = this._batch_operation[uid].func;

            var data_select = [];
            for (var i in this._data_slice) {
                var row = this._data_slice[i];
                if (row.select)
                    data_select.push(row);
            }
            if (func(data_select)) {
                // unclick all if function return true
                //show_toast('', '操作成功！', 500);
                //this._operate_on_thead.click_to_uncheck.bind(this)();
            }
        },
    }

    _operate_on_tbody = {
        click_row: function (e) {
            var tar = $(e.currentTarget);
            var uid = tar.attr('uid');

            if (uid == this._focus_row_index) {
                // un-focus target row
                this._focus_row_index = null;
                tar.css({'background-color': ''});

                if (this._unclickRowManual == null) return;
                else return this._unclickRowManual({});

            } else if (this._focus_row_index != null) {
                // un-focus focused row
                var old = this.jObj.find('tbody tr[uid=' + this._focus_row_index + ']');
                old.css({'background-color': ''});
                // focus current row
                this._focus_row_index = uid;
                tar.css({'background-color': '#FFF868'});

                if (this._clickRowManual == null) return;
                else return this._clickRowManual(this.getRowInfo(uid));
            } else {
                // focus current row
                this._focus_row_index = uid;
                tar.css({'background-color': '#FFF868'});

                if (this._clickRowManual == null) return;
                else return this._clickRowManual(this.getRowInfo(uid));
            }
        },

        click_to_check: function (e) {
            var tar = $(e.currentTarget);
            var col_obj = tar.closest('td');
            var row_obj = tar.closest('tr');
            var col_uid = col_obj.attr('uid');
            var row_uid = row_obj.attr('uid');

            for (var i in this._data_slice) {
                var row = this._data_slice[i];
                if (row['_table_data_uid_'] != row_uid) continue;
                row[col_uid] = true;
                break;
            }

            // open batch_operation here
            var jOpbox = this.jPannel.find('.batch_operation .textbox');
            var Nsel = parseInt(jOpbox.text());
            if (Nsel == 0)
                this._openBatchOperation();
            jOpbox.text(Nsel+1);
        }, 

        click_to_uncheck: function (e) {
            var tar = $(e.currentTarget);
            var col_obj = tar.closest('td');
            var row_obj = tar.closest('tr');
            var col_uid = col_obj.attr('uid');
            var row_uid = row_obj.attr('uid');

            for (var i in this._data_slice) {
                var row = this._data_slice[i];
                if (row['_table_data_uid_'] != row_uid) continue;
                row[col_uid] = false;
                break;
            }

            // open batch_operation here
            var jOpbox = this.jPannel.find('.batch_operation .textbox');
            var Nsel = parseInt(jOpbox.text());
            if (Nsel == 1)
                this._closeBatchOperation();
            jOpbox.text(Nsel-1);
        } 
    }

    _operate_on_thead = {
        sort_icon_none: function (e) {
            var tar = $(e.currentTarget);
            var col_obj = tar.closest('th');

            this.jObj.find('th .sort_icon_asc')
                .addClass('sort_icon_none')
                .removeClass('sort_icon_asc');
            this.jObj.find('th .sort_icon_desc')
                .addClass('sort_icon_none')
                .removeClass('sort_icon_desc');
            tar.addClass('sort_icon_desc').removeClass('sort_icon_none');

            var uid = col_obj.attr('uid');
            this.sortColumn(uid, 'desc');
        },

        sort_icon_desc: function (e) {
            var tar = $(e.currentTarget);
            var col_obj = tar.closest('th');

            this.jObj.find('th .sort_icon_asc')
                .addClass('sort_icon_none')
                .removeClass('sort_icon_asc');
            this.jObj.find('th .sort_icon_desc')
                .addClass('sort_icon_none')
                .removeClass('sort_icon_desc');
            tar.addClass('sort_icon_asc').removeClass('sort_icon_none');

            var uid = col_obj.attr('uid');
            this.sortColumn(uid, 'asc');
        },

        sort_icon_asc: function (e) {
            var tar = $(e.currentTarget);
            var col_obj = tar.closest('th');

            this.jObj.find('th .sort_icon_asc')
                .addClass('sort_icon_none')
                .removeClass('sort_icon_asc');
            this.jObj.find('th .sort_icon_desc')
                .addClass('sort_icon_none')
                .removeClass('sort_icon_desc');

            var uid = col_obj.attr('uid');
            this.sortColumn(uid, 'none');
        },

        click_to_check: function (e) {
            for (var i in this._data_slice) {
                var row = this._data_slice[i];
                row.select = true;
            }
            this._displayContent();

            console.log(this)
            // show batch_operation_block
            var jOpbox = this.jPannel.find('.batch_operation .textbox');
            var Nsel = parseInt(jOpbox.text());
            var Nsel_f = this._data_slice.length;
            if (Nsel == 0 && Nsel_f != 0)
                this._openBatchOperation();
            jOpbox.text(Nsel_f);
        }, 

        click_to_uncheck: function (e) {
            for (var i in this._data_slice) {
                var row = this._data_slice[i];
                row.select = false;
            }
            this._displayContent();

            // close batch_operation_block
            var jOpbox = this.jPannel.find('.batch_operation .textbox');
            var Nsel = parseInt(jOpbox.text());
            if (Nsel > 0)
                this._closeBatchOperation();
            jOpbox.text(0);
        } 
    }

    _frames = {
        frame: function () {
            var output = "";
            output += "<div class='" + this.style_cls + "'>"
            output += "    <div class=table_title></div>";
            output += "    <div class=table_pannel></div>";
            output += "    <div class=table_body>";
            output += "        <table>";
            output += "            <thead></thead>";
            output += "            <tbody></tbody>";
            output += "            <tfoot></tfoot>";
            output += "        </table>";
            output += "    </div>";
            output += "    <div class='myfooter'></div>";
            output += "</div>";
            return output;
        }.bind(this),

        title_bar_unit: function (cols) {
            var output = "";
            output += "<tr>";
            for (var i in cols) {
                var col = cols[i];
                var uid = col.uid
                var type = col.type;
                var format = col.format;
                var align = col.align;
                var sortable = col.sortable;
                var adjustable = col.adjustable;
                var value = col.value;

                var cls = (adjustable) ? 'adj' : 'fix';
                output += "<th uid='" + uid + "' class='" + cls + "'>";

                if (align == 'center')
                    output += "<div class=cell style='justify-content:center'>";
                else if (align == 'right')
                    output += "<div class=cell style='justify-content:flex-end'>";
                else if (align == 'left')
                    output += "<div class=cell style='justify-content:flex-start'>";
                else
                    output += "<div class=cell>"
                output += this._formatContent(type, value, format);
                if (sortable)
                    output += this._frames.sort_icon_unit('none');
                output += "</div>";
                output += "</th>";
            }
            output += "</tr>";
            return output;
        }.bind(this),

        row_unit: function (rowdata, highlight=false) {
            var output = "";
            var uid = rowdata['_table_data_uid_'];

            if (highlight)
                output += "<tr uid=" + uid + " style='background-color:#FFF868'>";
            else
                output += "<tr uid=" + uid + ">";

            for (var col of this._display_column_list) {
                var item = this._columns[col];
                var col_uid = item.name;
                var type = item.type;
                var format = item.format;
                var align = item.align;
                var adjustable = item.adjustable;
                var null_value = item.null_value;
                var value = rowdata[col];

                output += "<td uid='" + col_uid + "'  class=fix>";

                if (align == 'center')
                    output += "<div class=cell style='justify-content:center'>";
                else if (align == 'right')
                    output += "<div class=cell style='justify-content:flex-end'>";
                else if (align == 'left')
                    output += "<div class=cell style='justify-content:flex-start'>";
                else
                    output += "<div class=cell>";
                output += this._formatContent(type, value, format, null_value);
                output += "</div>";
                output += "</td>";
            }
            output += "</tr>";
            return output;
        }.bind(this),

        table_foot_unit: function () {
            var output = "";
            return output;
        },

        checkbox_unit: function (check) {
            var output = "";
            if (check)
                output = "<span class=checkbox_checked></span>";
            else
                output = "<span class=checkbox_unchecked></span>";
            return output;
        },

        sort_icon_unit: function (sort='none') {
            // none / asc / desc
            var cls = "";
            if (sort == 'asc')
                cls = 'sort_icon_asc';
            else if (sort == 'desc')
                cls = 'sort_icon_desc';
            else
                cls = 'sort_icon_none';

            var output = "";
            output += "<span class='" + cls + "'></span>";
            return output;
        },

        text_unit: function (text) {
            var output = "";
            output += "<span class=text>";
            output += text;
            output += "</span>";
            return output;
        }.bind(this),

        global_search_block: function () {
            var output = "";
            output += "<div class='global_search open'>";
            output += "    <span>搜尋：</span>";
            output += "    <span>";
            output += "        <input type=text value='' title='搜尋文字'>";
            output += "        <div class=css-close>";
            output += "            <div class=before></div>";
            output += "            <div class=after></div>";
            output += "        </div>";
            output += "    </span>";
            output += "</div>";
            return output;
        },

        batch_operation_block: function (batch_operation_objs) {
            var output = "";
            output += "<div class=batch_operation>";
            output += "    <div>";
            output += "        <span class=back_button>〈 返回/取消</span>";
            output += "        <span class=textbox>0</span>"
            output += "    </div>";
            output += "    <div>";

            for (var key in batch_operation_objs) {
                output += "<span class=manual_button uid='" + key + "'>";
                output += batch_operation_objs[key].html;
                output += "</span>";
            }
            output += "    </div>";
            output += "</div>";
            return output;
        },

        footer_block: function (current_page, total_page, num_display=2) {
            var output = "";

            if (num_display < 1) num_display = 1;
            var cls_first = 'click-enable';
            var cls_last = 'click-enable';

            if (current_page == 1)
                cls_first = 'click-disable';
            else if (current_page == total_page)
                cls_last = 'click-disable';

            var page_start = (current_page - num_display);
            var page_end = (current_page + num_display);

            if (total_page < num_display*2+1) {
                page_start = 1;
                page_end = total_page;
            }

            var shift = Math.max(1-page_start, 0) + Math.min(total_page-page_end, 0);
            page_start += shift;
            page_end += shift;

            output += "<div>";
            output += "<span class=" + cls_first + " tag='first'>第一頁</span>";
            output += "<span class=" + cls_first + " tag='prev'>上一頁</span>";
            if ((current_page-1) > num_display)
                output += "<span>...</span>";

            for (var i=page_start; i<=page_end; i++)
                if (i == current_page)
                    output += ("<span class='click-disable' tag='page'>" + i + "</span>");
                else
                    output += ("<span class='click-enable' tag='page'>" + i + "</span>");

            if ((total_page-current_page) > num_display)
                output += "<span>...</span>";
            output += "<span class=" + cls_last  + " tag='next'>下一頁</span>";
            output += "<span class=" + cls_last  + " tag='last'>最後一頁</span>";
            output += "</div>";
            return output;
        }

    }

    _create (jParent) {
        var sel = '.' + this.style_cls;
        if (jParent.find(sel).length != 0)
            jParent.find(sel).remove();
        jParent.append(this._frames.frame());
        var jObj = jParent.find(sel);
        return jObj;
    }

    _formatContent (type, value, format, null_value=null) {
        var content = "";
        if ((type != 'checkbox') && (value == null) && (null_value == null))
            throw 'Value of cannot be null. Please check input data or set a none-null null_value';
        value = (value == null) ? null_value : value;

        if (type == "checkbox") {
            content = this._frames.checkbox_unit(value);
        } else if (type == "int") {
            content = this._frames.text_unit(this._formatNumber(value, "%d"));
        } else if (type == "float") {
            content = this._frames.text_unit(this._formatNumber(value, format));
        } else if (type == "datetime") {
            content = this._frames.text_unit(this._formatDatetime(value, format));
        } else {
            content = this._frames.text_unit(''+value);
        }
        return content;
    }

    _formatNumber (value, format) {
        if (value == null) return '';
        if (isNaN(value)) return value;

        var reg1 = /^%(?:0(\d+)){0,1}(?:\.(\d+)){0,1}([f|e|d])$/;
        if (format == null) return value.toString();

        var pat = format.match(reg1);
        if (pat == null) return value.toString();

        var val = parseFloat(value);
        var output = '';
        if (pat[3] == 'e') {
            var n = Math.floor(Math.log(value)/Math.log(10));
            output = 'e' + n;
            val /= Math.pow(10, n);
        } else if (pat[3] == 'd') {
            pat[2] = '0';
        }
        var reg2 = /^(?:(-{0,1}\d+))(?:.(\d+)){0,1}$/;
        var d = val.toFixed(8).match(reg2);

        pat[1] = parseInt(pat[1]);
        pat[2] = parseInt(pat[2]);

        if (isNaN(pat[2])) output = '.' + d[2] + output;
        else if (pat[2] > 0)
            output = '.' + (d[2].length >= pat[2]) ? d[2].slice(0, pat[2]) : (d[2]+'0'.repeat(pat[2]-d[2].length)) + output;

        if (isNaN(pat[1])) output = d[1] + output;
        else output = '' + ((d[1].length >= pat[1]) ? d[1] : ('0'.repeat(pat[1]-d[1].length)+d[1])) + output;

        return output;
    }

    _formatDatetime (value, format='yyyy-MM-dd') {
        // value should be object of Date
        // y -> year
        // M -> month
        // d -> day
        // h -> hour
        // m -> minute
        // s -> second
        if (value == null) return '';
        if (!(value instanceof Date)) return value;

        var output = format;
        output = output.replace(/(y+)/g, this._formatNumber(value.getFullYear(), '%04d'));
        output = output.replace(/(M+)/g, this._formatNumber(value.getMonth()+1 , '%02d'));
        output = output.replace(/(d+)/g, this._formatNumber(value.getDate()    , '%02d'));
        output = output.replace(/(h+)/g, this._formatNumber(value.getHours()   , '%02d'));
        output = output.replace(/(m+)/g, this._formatNumber(value.getMinutes() , '%02d'));
        output = output.replace(/(s+)/g, this._formatNumber(value.getSeconds() , '%02d'));
        return output;
    }

    _initialize (setting={}) {
        // set all except data (rows)
        this._title = '';
        this._display_title = false;
        this._display_in_page = false;
        this._display_num_rows = 'auto';
        this._display_column_list = [];
        this._display_table_foot = false;
        this._clickRowManual = null;
        this._unclickRowManual = null;
        this._global_search = true;
        this._columns = {};
        this._data = [];
        this._batch_operation = {};
        /*
        this._batch_operation: 
            key: {
                key: <key>,
                html: <html to create the button>,
                func: <function to call when the button was clicked>
            }
        */
        this._keyword = '';

        this.jTitle = this.jObj.find('.table_title');
        this.jPannel = this.jObj.find('.table_pannel');
        this.jTable = this.jObj.find('table');
        this.jFooter = this.jObj.find('.myfooter');

        this.updateSetting(setting);
    }

    getColumnFunctionList () {
    }

    getRowFunctionList () {
    }

    _display () {
        this._displayTitle();
        this._displayTableHead();
        this._displayTablePannel();
        this._displayTableFoot();
        this._displayContent();
    }

    _displayTitle () {
        this.jTitle.empty();

        if (this._display_title)
            this.jTitle.append(this._title);
    }

    _displayTablePannel () {
        this.jPannel.empty();

        if (this._global_search)
            this.jPannel.append(this._frames.global_search_block());

        this.jPannel.append(this._frames.batch_operation_block(this._batch_operation));
    }

    _openBatchOperation () {
        this.jPannel.find('.global_search.open').removeClass('open');
        this.jPannel.find('.batch_operation').addClass('open');
    }
    
    _closeBatchOperation () {
        this.jPannel.find('.batch_operation.open').removeClass('open');
        this.jPannel.find('.global_search').addClass('open');
    }
    
    _displayTableHead () {
        var sel = 'thead';
        var jObj = this.jTable.find(sel);
        jObj.empty();

        var cols = [];
        for (var key of this._display_column_list)
            if (this._columns[key] != null)
                cols.push({
                    uid: this._columns[key].name,
                    type: (this._columns[key].type == 'checkbox') ? 'checkbox' :'text',
                    format: null,
                    value: this._columns[key].title,
                    align: this._columns[key].align,
                    adjustable: this._columns[key].adjustable,
                    sortable: this._columns[key].sortable
                })

        if (cols.length > 0)
            jObj.append(this._frames.title_bar_unit(cols));
    }

    _displayTableFoot () {
        var sel = 'tfoot';
        var jObj = this.jTable.find(sel);
        jObj.empty();

        if (this._display_table_foot)
            jObj.append(this._frames.table_foot_unit());
    }

    _displayContent (page) {
        var jObj = this.jTable.find('tbody');
        jObj.empty();
        
        if (this._data_slice.length == 0) {
            jObj.append("<span style='position:absolute'>(Empty)</span>");
            return;
        }

        if (page == null && this._current_page == null) {
            page = 1;
        } else if (page == null && this._current_page != null)
            page = this._current_page;

        var num = this._display_num_rows;
        var slice;
        if (this._display_num_rows == 'auto') {
            // auto paging
            num = 18;
            var body = this.jObj.find('.table_body');
            var thead = this.jObj.find('thead')
            if (body.outerHeight() > 0 && thead.outerHeight() > 0)
                num = Math.floor((body.outerHeight()-10)/thead.outerHeight()) - 1;
        }

        if (this._display_in_page) {
            this._num_pages = Math.ceil(this._data_slice.length / num)
            if (page > this._num_pages) page = this._num_pages;
            else if (page == -1) page = this._num_pages;
            else if (page < 1) page = 1;

            slice = this._data_slice.slice((page-1)*num, page*num);
        } else {
            this._num_pages = 1;
            this._current_page = 1;
            slice = this._data_slice;
        }

        for (var i in slice) {
            var row_data = slice[i];

            var highlight = (row_data['_table_data_uid_'] == this._focus_row_index);
            var row_unit = this._frames.row_unit(row_data, highlight);
            jObj.append(row_unit);
        }

        this._current_page = page;
        this._displayFooter(this._current_page, this._num_pages);
    }

    _displayFooter (current_page, total_page) {
        this.jFooter.empty();

        if (this._display_in_page)
            this.jFooter.append(this._frames.footer_block(current_page, total_page, 2));
    }

    setData (data) {
        this._data = [];
        if (data != null)
            if (typeof data == 'object')
                for (var i in data)
                    this._data.push($.extend(true, {'_table_data_uid_': parseInt(i)}, data[i]));

        this._data_slice = this._data;
        this._keyword = null;
        this._displayContent(1);
    }

    updateSetting (setting={}) {
        if (setting['display_in_page'] != null)
            this._display_in_page = Boolean(setting['display_in_page']);
        
        if (setting['title'] != null)
            this._title = setting['title'];

        if (setting['display_title'] != null)
            this._display_title = Boolean(setting['display_title']);

        if (setting['display_table_foot'] != null)
            this._display_table_foot = Boolean(setting['display_table_foot']);

        if (setting['global_search'] != null)
            this._global_search = Boolean(setting['global_search']);

        if (!isNaN(parseInt(setting['display_num_rows'])))
            this._display_num_rows = parseInt(setting['display_num_rows']);

        if (setting['display_column_list'] != null)
            if (typeof setting['display_column_list'] == 'object')
                this._display_column_list = $.extend(true, [], setting['display_column_list']);

        if (setting['clickRowManual'] != null)
            if (typeof setting['clickRowManual'] == 'function')
                this._clickRowManual = setting['clickRowManual'];

        if (setting['unclickRowManual'] != null)
            if (typeof setting['unclickRowManual'] == 'function')
                this._unclickRowManual = setting['unclickRowManual'];

        if (setting['columns'] != null)
            if (typeof setting['columns'] == 'object')
                for (var key in setting['columns'])
                    this._columns[key] = $.extend(true, {}, setting['columns'][key]);

        if (setting['data'] != null)
            if (typeof setting['data'] == 'object') {
                this._data = [];
                for (var i in setting['data'])
                    this._data.push($.extend(true, {'_table_data_uid_': parseInt(i)}, setting['data'][i]));
            }

        if (setting['batch_operation'] != null) {
            if (typeof setting['batch_operation'] == 'object') {
                this._batch_operation = {};
                for (var key in setting['batch_operation'])
                    this._batch_operation[key] = {
                        key: key,
                        html: setting['batch_operation'][key].html,
                        func: setting['batch_operation'][key].func
                    }
            }
        }

        this._data_slice = this._data;
        this._display();
    }

    insertRow () {
    }

    insertRows () {
    }

    deleteRow () {
    }

    deleteRows () {
    }

    editRow () {
    }

    editRows () {
    }

    insertColumn () {
    }

    hideColumn () {
    }

    hideColumns () {
    }

    moveColumn () {
    }

    moveColumns () {
    }

    sortColumn (column_uid, option='asc') {
        var type = this._columns[column_uid].type;
        var von;
        if (type == 'checkbox') {
            von = false;
        } else if (type == 'int') {
            von = -9999;
        } else if (type == 'float') {
            von = -9999.;
        } else if (type == 'text') {
            von = '';
        } else {
            von = '';
        }

        if (option == 'asc') {
            this._data_slice = this._data_slice.sort(function (obj1, obj2) {
                var col = column_uid;
                var v1 = (obj1[col] == null) ? von : obj1[col];
                var v2 = (obj2[col] == null) ? von : obj2[col];
                return v1 > v2 ? 1 : -1;
            });

        } else if (option == 'desc') {
            this._data_slice = this._data_slice.sort(function (obj1, obj2) {
                var col = column_uid;
                var v1 = (obj1[col] == null) ? von : obj1[col];
                var v2 = (obj2[col] == null) ? von : obj2[col];
                return v1 > v2 ? -1 : 1;
            });

        } else {
            this._data_slice = this._data_slice.sort(function (obj1, obj2) {
                var col = '_table_data_uid_';
                var v1 = (obj1[col] == null) ? von : obj1[col];
                var v2 = (obj2[col] == null) ? von : obj2[col];
                return v1 > v2 ? 1 : -1;
            });
        }
        this._displayContent(1);
    }

    filterColumn () {
    }

    filter (text) {
        if (text == '' || text == null) {
            this._data_slice = this._data;
            this._keyword = null;
            this._displayContent(1);
            return
        }

        text = text.replace(/^\ /, '');
        text = text.replace(/\ $/, '');
        text = text.replace(/[(\.)(\\)(\%)(\$)(\-)(\/)(\ )]/g, '\\$&');

        var dataset = this._data;

        if (this._keyword != null) {
            var reg = new RegExp(this._keyword);
            if (text.match(reg) != null)
                dataset = this._data_slice;
        }

        reg = new RegExp(text);

        var new_slice = [];
        for (var row of dataset) {
            for (var i in this._display_column_list) {
                var col = this._display_column_list[i];
                if (row[col] == null) continue;

                var type = this._columns[col].type;
                var format = this._columns[col].format;
                var value =  row[col];
                var content = '';

                if (type == 'checkbox') continue;
                else if (type == "int")
                    content = this._formatNumber(parseInt(value), '%d');
                else if (type == "float")
                    content = this._formatNumber(value, format);
                else if (type == "datetime")
                    content = this._formatDatetime(value, format);
                else
                    content = '' + value;

                if (content.match(reg) != null) {
                    new_slice.push(row);
                    continue;
                }
            }
        }
        this._data_slice = new_slice;
        this._keyword = text;
        this._displayContent(1);
    }

    adjustColumnWidth () {
    }

    autoColumnWidth () {
    }

    getRowInfo (uid) {
        for (var row of this._data_slice) {
            if (row['_table_data_uid_'] != uid) continue;
            return row;
        }
    }

    nextPage () {
        return this.toPage(this._current_page+1);
    }

    prevPage () {
        return this.toPage(this._current_page-1);
    }

    firstPage () {
        return this.toPage(1);
    }

    lastPage () {
        return this.toPage(this._num_pages);
    }

    toPage(page_num=1) {
        return this._displayContent(page_num);
    }
}
