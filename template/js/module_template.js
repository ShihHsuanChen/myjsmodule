class ModuleTemplate {
    constructor (jHidden_id) {
        var id = jHidden_id;
        if (id == null)
            id = 'id_module_template_default_hidden';

        var jHidden = $('body').find('#' + id);
        if (jHidden.length == 0) {
            $('body').append(this._get_frame().get_hidden(id));
            jHidden = $('body').find('#' + id)
        }
        this.jHidden = jHidden;
        this.m_dialog = null;
        this.jParent = null;
    }

    bindEvents () {
    }

    unbindEvents () {
    }

    _get_frame () {
        // inhirit this function:
        // _get_frame () {
        //     // define manual functions here
        //     var frames = {
        //         frame1: fucntion (...) {},
        //         frame2: fucntion (...) {},
        //     }
        //     return $.extend({}, super._get_frame(), frames)
        // }
        return {
            get_hidden: function (id) {
                var output = "";
                output += "<div id='" + id + "' style='display:none;width:0px;height:0pxloverflow:hidden;'></div>"
                return output;
            }
        };
    }

    destroy () {
        if (this.m_dialog != null)
            this.close_dialog();
        this.unbindEvents();
    }

    set_parent (jParent) {
        this.jParent = jParent;
    }

    get_parent () {
        return this.jParent;
    }

    open_dialog (setting=null, dialog_styles=null) {
        var destroy_func = this.close_dialog.bind(this);
        var dialog_setting_default = {
            autoOpen: false,
            draggable: true,
            resizable: true,
            modal: true,
            height: 500,
            width: 500,
            title: 'search element',
            close: destroy_func,
            closeOnEscape: false
        };

        if ((setting != null) && (typeof setting == 'object'))
            this.dialog_setting = $.extend({}, dialog_setting_default, setting);
        else if (setting == 'default' || this.dialog_setting == null)
            this.dialog_setting = dialog_setting_default;

        $.ui.dialog.prototype._focusTabbable = function(){};
        this.m_dialog = $("<div></div>").dialog(this.dialog_setting);

        this.m_dialog.dialog('open');
        var jDialog = this.m_dialog.parent();

        jDialog.css({"background": "#E2E2E2"})
        jDialog.find(".ui-dialog-titlebar-close").css('display', 'block');
        jDialog.find(".ui-dialog").css({"padding": "0px"});
        jDialog.find(".ui-dialog .ui-dialog-content").css({"padding": "0px", "background": "#E2E2E2"});
        
        if (dialog_styles != null) {
            for (var sel in dialog_styles) {
                var style = dialog_styles[sel];
                if (sel == '.')
                    jDialog.css(style);
                else {
                    var jobj = jDialog.find(sel);
                    if (jobj.length == 0) continue;
                    jobj.css(style);
                }
            }
        }
    }

    close_dialog () {
        this.dialog_setting.width = this.m_dialog.parent().outerWidth();
        this.dialog_setting.height = this.m_dialog.parent().outerHeight();
        this.m_dialog.dialog('destroy').remove();
        this.m_dialog = null;
    }

    get_dialog () {
        return this.m_dialog;
    }
}
