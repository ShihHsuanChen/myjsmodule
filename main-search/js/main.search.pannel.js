class MainSearchPannel extends MainSearchElement {
    constructor (jHidden_id, setting) {
        // setting: {
        //     searchSubmit: function to execute when submit button is clicked
        // }
        super(jHidden_id);
        this.jParent = this.jHidden;

        this.style_cls = "main-search-pannel";
        var sel = "." + this.style_cls;
        
        this._loading_dialog = null;
        this.itemlist = ['project_name', 'patient_id', 'study_id', 'exam_date', 'report_text'];
        this.jObj = this.create(this.itemlist, 'vertical');
        this._searchSubmit = setting['searchSubmit'];
        this._user = setting['user'];
        this._project_list = [];
        this._get_options();

        this.bindEvents();
    }

    bindEvents () {
        this.jObj.ready(
            textboxHint.bind(null, this.jObj)
        );
        this.jObj.on('click', '.checkbox_unchecked', function () {
            $(this).addClass('checkbox_checked').removeClass('checkbox_unchecked');
        });
        this.jObj.on('click', '.checkbox_checked', function () {
            $(this).addClass('checkbox_unchecked').removeClass('checkbox_checked');
        });
        this.jObj.on('click', 'button', this.submit.bind(this));
        this.jObj.on('change', 'input[type=date]',
            this._operate_date_input.change.bind(this)
        );
    }

    unbindEvents () {
        super.unbindEvents();
    }

    _operate_date_input = {
        change: function (e) {
            var tar = $(e.currentTarget);
            var value = tar.val();
            var name = tar.attr('name');
            var jTop = tar.closest('span.input');
            if (name == 'start')
                jTop.find('input[type=date][name=end]').attr('min', value);
            else if (name == 'end')
                jTop.find('input[type=date][name=start]').attr('max', value);
        },
    }

    _items = {
        project_name: { uid: 'project_name', 
            group: 'study', title: '專案名稱', type: 'menu', check: 'always', option: null
        },
        patient_id: { uid: 'patient_id',
            group: 'study', title: 'Patient ID', type: 'input', check: 'auto', hint: 'example: 83954'
        },
        study_id: { uid: 'study_id',
            group: 'study', title: 'Study ID', type: 'input', check: 'auto', hint: 'example: 20548'
        },
        exam_date: { uid: 'exam_date',
            group: 'study', title: 'Exam Date', type: 'calendar_range', check: 'auto'
        },

        report_text: { uid: 'report_text',
            group: 'study', title: '報告文字', type: 'input', check: 'auto', hint: 'example: nodule'
        },

        labeler: { uid: 'labeler',
            group: 'task', title: '標註者', type: 'menu', check: 'auto', option: null
        },
        confirmer: { uid: 'confirmer',
            group: 'task', title: '確認者', type: 'menu', check: 'auto', option: null
        },
        label_status: { uid: 'label_status',
            group: 'task', title: '標註狀態', type: 'menu', check: 'auto', option: {0: '未標註', 1: '已標註'}
        },
        confirm_status: { uid: 'confirm_status',
            group: 'task', title: '確認狀態', type: 'menu', check: 'auto', option: {0: '未確認', 1: '已確認'}
        },
        label_date: { uid: 'label_date',
            group: 'task', title: ' 標註日期 ', type: 'calendar_range', check: 'auto'
        },
        confirm_date: { uid: 'confirm_date',
            group: 'task', title: '確認日期', type: 'calendar_range', check: 'auto'
        }
    }

    _get_frame () {
        var frames = {
            frame: function (layout) {
                var cls = (layout=='horizontal') ? 'frame-horizontal' : 'frame-vertical';
                var output = "";
                output += "<div class='" + this.style_cls + "'>";
                output += "    <div class=pannel-title>請勾選並輸入搜尋條件</div>";
                output += "    <div class=" + cls + ">";
                output += "        <div class=container group=study></div>";
                output += "        <div class=container group=task></div>";
                output += "    </div>";
                output += "    <button>Submit</button>"
                output += "</div>";
                return output; 
            }.bind(this),

            criteria_line_unit: function (uid, group, title, check_stat, check=false, hint='', options=null) {
                if (hint == null) hint = '';
                var ckcls = (check_stat=='always') ? 'checkbox_fixed' : ((check) ? 'checkbox_checked' : 'checkbox_unchecked');

                var output = "";
                output += "<div class=criteria_line_unit group='" + group + "' uid=" + uid + ">";
                output += "    <span class='" + ckcls + "'></span>";
                output += "    <span class='title'>" + title + "</span>";
                output += "    <span class='input'>"

                if (group == 'input') 
                    output += "    <input type='text' value='' title='" + hint + "'/>";
                else if (group == 'menu') {
                    var id = 'select_list_' + uid;
                    if (options == null) options = [];
                    output += "    <select id='" + id + "' title='" + hint + "'>";
                    output += this._get_frame().select_option(options);
                    output += "    </select>";
                } else if (group == 'calendar_range') {
                    output += "<div>";
                    output += "<span>From</span>";
                    output += "<input type='date' name=start max='2199-12-31' min='1988-01-01'>";
                    output += "</div>";
                    output += "<div>";
                    output += "<span>To</span>";
                    output += "<input type='date' name=end max='2199-12-31' min='1988-01-01'>";
                    output += "</div>";
                }

                output += "    </span>";
                output += "</div>";
                return output; 
            }.bind(this),

            select_option: function (options) {
                var output = "";
                for (var opt of options)
                    output += "<option value='" + opt + "'>" + opt + "</option>";
                return output; 
            }
            //frame1: fucntion (...) {},
            //frame2: fucntion (...) {},
        }
        return $.extend({}, super._get_frame(), frames)
    }

    // api
    _get_options () {
        this._api_get_project_list();
        this._api_get_user_list();
    }

    _api_get_project_list () {
        var req = {};
        if (this._user != null) req['email1'] = this._user;

        this._loading_dialog = new LoadingDialog(
            'id_loading_1', 200, 100, '資料載入中', "",
            this.update_option.bind(this, 'project_name')
        );

        $.post(ip_port_ui + "get_project_list", req, function (data) {
            var res = JSON.parse(data);

            if (res['status'][0] == 100) {
                var tmp = {};
                this._project_list = [];

                for (var obj of res['project_list'])
                    tmp[obj.project_id] = obj.project_name;
                for (var key in tmp)
                    this._project_list.push({'project_id': key, 'project_name': tmp[key]})
            } else 
                throw "/get_project_list : " + res['status'][0] + ' : ' + res['status'][1] + ' : ' + res['status'][2];
        }.bind(this)).done(function () {
            var tmp = [];
            for (var obj of this._project_list) tmp.push(obj.project_name);
            this._items['project_name'].option = tmp;
            this._loading_dialog.close();
            this._loading_dialog = null;
        }.bind(this));
    }

    _api_get_user_list () {
        var req = {};

        this._loading_dialog_1 = new LoadingDialog(
            'id_loading_2', 200, 100, '資料載入中', "", function () {
                this.update_option('labeler');
                this.update_option('confirmer');
            }.bind(this)
        );

        $.post(ip_port_ui + "get_user_list", req, function (data) {
            var res = JSON.parse(data);

            if (res['status'][0] == 100) {
                var tmp = {};
                this._user_list = [];

                for (var obj of res['user_list'])
                    tmp[obj.email1] = obj.name1;
                for (var key in tmp)
                    this._user_list.push({'email': key, 'name': tmp[key]})
            } else 
                throw "/get_user_list : " + res['status'][0] + ' : ' + res['status'][1] + ' : ' + res['status'][2];
        }.bind(this)).done(function () {
            var tmp = [];
            for (var obj of this._user_list) tmp.push(obj.name);
            this._items['labeler'].option = tmp;
            this._items['confirmer'].option = tmp;
            this._loading_dialog_1.close();
            this._loading_dialog_1 = null;
        }.bind(this));
    }

    _api_search_study (req) {

        this._loading_dialog_2 = new LoadingDialog('id_loading_2', 200, 100, '資料載入中', "");

        $.post(ip_port_ui + "search_study", req, function (data) {
            var res = JSON.parse(data);

            if (res['status'][0] == 100) {
                var study_list = res['study_list'];
                var user_info = res['users'];
                var project_info = res['projects'];
            } else 
                throw "/search_study : " + res['status'][0] + ' : ' + res['status'][1] + ' : ' + res['status'][2];

            this._searchSubmit(req, study_list)

        }.bind(this)).done(function () {
            this._loading_dialog_2.close();
            this._loading_dialog_2 = null;
        }.bind(this));
    }

    create (itemlist, layout='vertical') {
        var sel = "." + this.style_cls;
        if (this.jParent.find(sel).length != 0) {
            this.jParent.find(sel).remove();
        }

        var frame = this._get_frame().frame(layout);
        this.jParent.append(frame);

        var jObj = this.jParent.find(sel)
        var items = this._items;
        for (var it of itemlist) {
            var item = items[it];
            if (item == null) continue;
            var uid = item.uid;
            var type = item.type;
            var title = item.title;
            var check_stat = item.check;

            var line = '';
            if (type == 'input') {
                var hint = item.hint;
                line = this._get_frame().criteria_line_unit(uid, type, title, check_stat, false, hint)

            } else if (type == 'menu') {
                var opts = null;
                if (item.option != null) {
                    opts = [];
                    for (var j in item.option)
                        opts.push(item.option[j]);
                }
                line = this._get_frame().criteria_line_unit(uid, type, title, check_stat, false, 'Click to select', opts)

            } else if (type == 'calendar_range') {
                line = this._get_frame().criteria_line_unit(uid, type, title, check_stat, false)
            }

            var grp = item.group;
            jObj.find('.container[group=' + grp + ']').append(line);
        }
        return jObj;
    }

    update_option (item_name) {
        var item = this._items[item_name];
        if (item == null)
            throw "cannot find item named '" + item_name + "'";
        if (item.type != 'menu')
            throw "Type of item '" + item_name + "' is not 'menu'";
        var uid = item.uid;
        var opt = item.option;
        if (opt == null) return;
        var jSelect = this.jObj.find('.criteria_line_unit[uid=' + uid + '] select');
        jSelect.empty();
        jSelect.append(this._get_frame().select_option(opt));
    }

    get_criterian () {
        var criterian = {};

        for (var it of this.itemlist) {
            var item = this._items[it];

            var jLineunit = this.jObj.find('.criteria_line_unit[uid=' + it + ']')
            if (jLineunit.find('.checkbox_unchecked').length != 0) continue;

            var value = null;
            if (item.type == 'input') {
                if (jLineunit.find('input.blur').length!=0) {
                    alert ('Selected item "' + item.title + '" has no value');
                    return null;
                }
                criterian[it] = jLineunit.find('input')[0].value;
            } else if (item.type == 'menu') {
                if (jLineunit.find('select')[0].selectedIndex == -1) {
                    alert ('Selected item "' + item.title + '" has no value');
                    return null;
                }
                criterian[it] = jLineunit.find('select')[0].value;
            } else if (item.type == 'calendar_range') {
                var date_start = jLineunit.find('input[name=start]')[0].value;
                var date_end = jLineunit.find('input[name=end]')[0].value;
                if (date_start == "" && date_end == "") {
                    alert ('At least one date (start or end) of selected item "' + item.title + '" should be selected');
                    return null;
                }
                if (date_start != "")
                    criterian[it+'_start'] = date_start;
                if (date_end != "")
                    criterian[it+'_end'] = date_end;
            }
        }
        return criterian;
    }

    submit () {
        var criterian = this.get_criterian();
        if (criterian == null) return;

        if (Object.keys(criterian).length === 0) {
            alert("請至少選擇一項搜尋條件");
            return;
        }

        var req = {};
        for (var it in criterian) {
            if (it == 'project_name') {
                var id = null;
                for (var obj of this._project_list)
                    if (obj.project_name == criterian[it]) {
                        req['project_id'] = obj.project_id;
                        break;
                    }
            } else if (it == 'labeler'){
                var id = null;
                for (var obj of this._user_list)
                    if (obj.name1 == criterian[it]) {
                        req['labeler_email'] = obj.email1;
                        break;
                    }
            } else if (it == 'confirmer'){
                var id = null;
                for (var obj of this._user_list)
                    if (obj.name1 == criterian[it]) {
                        req['confirmer_email'] = obj.email1;
                        break;
                    }
            } else if (it == 'report_text'){
                req['keyword_include_all'] = criterian[it];
            } else {
                req[it] = criterian[it];
            }
        }
        
        this._api_search_study(req);
    }
}
