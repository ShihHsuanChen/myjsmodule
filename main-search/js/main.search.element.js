class MainSearchElement extends ModuleTemplate {
    constructor (jHidden_id) {
        super(jHidden_id);
        this.jParent = this.jHidden;
        this.dialog_setting = null;
    }

    bindEvents () {
    }

    unbindEvents () {
        this.jObj.off();
        this.jObj.unbind();
    }

    _get_frame () {
        return super._get_frame()
    }

    destroy () {
        this.jObj.remove();
        super.destroy();
    }

    set_parent (jParent) {
        if (this.jObj == null)
            throw 'Cannot set parent of this.jObj (null)';
        if (jParent == null)
            throw 'Cannot set null as parent';
        this.jObj.detach().appendTo(jParent);
        return super.set_parent;
    }

    detach () {
        this.set_parent(this.jHidden);
    }

    open_dialog (setting=null, dialog_styles=null) {
        super.open_dialog(setting, dialog_styles);
        this.set_parent(this.m_dialog);
    }

    close_dialog () {
        if (this.get_dialog() == null) return;
        this.detach();
        super.close_dialog();
    }
}
