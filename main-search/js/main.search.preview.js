class MainSearchPreview extends MainSearchElement {
    constructor (jHidden_id) {
        super(jHidden_id);
        this.jParent = this.jHidden;

        this.style_cls = "main-search-preview";
        var sel = "." + this.style_cls;
        
        this.jObj = this.create();
        this.jPannel = this.jObj.find('.pannel');
        this.jInfo = this.jObj.find('.info-content');
        this.jImage = this.jObj.find('.image-content');
        this.bindEvents();
    }

    bindEvents () {
        this.jObj.ready(function () {
            /*
            $(this.jInfo.parent()).resizable({handles: "s"});
            */
        }.bind(this));

    }

    unbindEvents () {
        super.unbindEvents();
    }

    unbindPannelEvents () {
        this.jPannel.off();
        this.jPannel.unbind();
    }

    _get_frame () {
        var frames = {
            frame: function (layout) {
                var output = "";
                output += "<div class='" + this.style_cls + "'>"
                output += "    <div class=pannel></div>";
                output += "    <div class=info-content-wrap>";
                // text container here
                output += "        <div class=info-content></div>";
                output += "    </div>";
                output += "    <div class=image-content-wrap>";
                // image container here
                output += "        <div class=image-content></div>";
                output += "    </div>";
                output += "</div>";
                return output; 
            }.bind(this),

            info_content_unit: function (data, disp_group_list=null) {
                // show in one group when disp_group_list == null
                // else put data in rows w.r.t. its group
                var output = "";
                output += "<div class=info_title></div>"
                if (disp_group_list != null) {
                    for (var grp of disp_group_list)
                        output += this._get_frame().info_group_unit(
                            grp, this._getDataSlice(data, grp)
                        );
                } else {
                    output += this._get_frame().info_group_unit(
                        null, data
                    );
                }
                return output;
            }.bind(this), 

            info_group_unit: function (grp_name, data) {
                var output = "";
                output += "<div uid='" + grp_name + "'>";
                var showdata = {};
                for (var i in data) {
                    var tmp = {};
                    showdata[data[i].title] = data[i].value;
                }
                output += myAccordionTableUnit(showdata);
                output += "</div>";
                return output;
            }.bind(this),

            info_list_unit: function (data) {
                if (data == null)
                    throw "input data should be an Array (null given)";
                if (data.constructor.name != 'Array')
                    throw "input data should be an Array (" + data.constructor.name + " was given)";
                var text_children = false;
                for (var elem of data)
                    text_children |= (typeof elem != 'object');

                console.log(data)

                var output = "";
                if (text_children) {
                    output += "<ul>"
                    for (var elem of data) {
                        output += "<li>";
                        output += this._parseContent(elem);
                        output += "</li>";
                    }
                    output += "</ul>"
                } else {
                    var list_data = [];
                    for (var i in data)
                        list_data.push(this._parseContent(data[i]));
                    output += myAccordionUnit(list_data);
                }
                return output;
            }.bind(this),

            info_dict_unit: function (data) {
                if (data == null)
                    throw "input data should be an Object (null given)";
                if (data.constructor.name != 'Object')
                    throw "input data should be an Object (" + data.constructor.name + " was given)";

                var output = "";
                output += "<table>";
                for (var key in data) {
                    output += "<tr>"
                    output += "    <td class=info_key>" + key + "</td>";
                    output += "    <td class=info_value>";
                    output += this._parseContent(data[key]);
                    output += "    </td>";
                    output += "</tr>"
                }
                output += "</table>";
                return output;
            }.bind(this),

            image_content_unit: function () {
                var output = "";
                return output;
            }, 
            //frame1: fucntion (...) {},
            //frame2: fucntion (...) {},
        }
        return $.extend({}, super._get_frame(), frames)
    }

    _parseContent (data) {
         if (data == null)
             return "";
         else if (data.constructor.name == 'Array')
             return this._get_frame().info_list_unit(data);
         else if (data.constructor.name == 'Object')
             return this._get_frame().info_dict_unit(data);
         else
             return data;
    }

    _getDataSlice (data, group, exclude=false) {
        var slice = [];
        for (var i in data) {
            if (exclude) {
                if (data[i]['group'] == group) continue;
            } else {
                if (data[i]['group'] != group) continue;
            }
            slice.push(data[i]);
        }
        return slice;
    }

    create (itemlist) {
        var sel = "." + this.style_cls;
        if (this.jParent.find(sel).length != 0) {
            this.jParent.find(sel).remove();
        }

        var frame = this._get_frame().frame();
        this.jParent.append(frame);

        var jObj = this.jParent.find(sel)
        return jObj;
    }

    clear () {
        this.jPannel.empty();
        this.jInfo.empty();
        this.jImage.empty();
        this.unbindPannelEvents();
    }

    showData (data) {
        // pannel
        // info
        var tmp = {};
        for (var i in data)
            tmp[data[i]['group']] = '';
        var group_list = Object.keys(tmp);
        
        this.clear();
        this.jInfo.append(
            this._get_frame().info_content_unit(
                this._getDataSlice(data, 'image', true),
                group_list
            )
        );
        /*
        myAccordionActivate($('.myaccordion'));
        */
        // image
        myAccordionTableActivate($('.myaccordion_table'));

        this.jImage.append(
            this._get_frame().image_content_unit(
                this._getDataSlice(data, 'image', false)
            )
        );
    }
}
