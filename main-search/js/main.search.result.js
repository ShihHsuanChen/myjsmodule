class MainSearchResult extends MainSearchElement {
    constructor (jHidden_id, setting) {
        super(jHidden_id);
        this.jParent = this.jHidden;

        this.style_cls = "main-search-result";
        var sel = "." + this.style_cls;
        
        this.default_setting = {
            title: 'Search Result',
            display_title: false,
            display_in_page: true,
            display_table_foot: false,
            display_num_rows: '20',
            display_column_list: ['select', 'study_id', 'body_part', 'modality', 'patient_id', 'exam_date', 'source'],
            global_search: true,
            batch_operation: this._batch_operation
        }

        var setting = $.extend({}, {columns: this._columns}, this.default_setting, setting) ;
        this.jObj = this.create();
        this.table = new myTable(this.jObj.find('.result_table'), setting);
        this.bindEvents();
    }

    bindEvents () {
        this.jObj.ready(
            textboxHint.bind(null, this.jObj)
        );
        this.jObj.on('click', '.checkbox_unchecked', function () {
            $(this).addClass('checkbox_checked').removeClass('checkbox_unchecked');
        });
        this.jObj.on('click', '.checkbox_checked', function () {
            $(this).addClass('checkbox_unchecked').removeClass('checkbox_checked');
        });
    }

    unbindEvents () {
        super.unbindEvents();
    }

    _columns = {
        'select': {
            name: 'select',
            title: null,
            type: 'checkbox',
            null_value: null,
            format: null,
            sortable: true,
            adjustable: false,
            filter: false,
            align: 'center'
        },
        'study_id': {
            name: 'study_id',
            title: 'Study ID',
            type: 'int',
            null_value: null,
            format: null,
            sortable: true,
            adjustable: true,
            filter: false,
            align: 'center'
        },
        'patient_id': {
            name: 'patient_id',
            title: 'Patient ID',
            type: 'int',
            null_value: null,
            format: null,
            sortable: true,
            adjustable: true,
            filter: false,
            align: 'center'
        },
        'modality': {
            name: 'modality',
            title: 'Modality',
            type: 'text',
            null_value: '(unknown)',
            format: null,
            sortable: true,
            adjustable: true,
            filter: false,
            align: 'center'
        },
        'body_part': {
            name: 'body_part',
            title: '部位',
            type: 'text',
            null_value: '(unknown)',
            format: null,
            sortable: true,
            adjustable: true,
            filter: false,
            align: 'center'
        },
        'exam_date': {
            name: 'exam_date',
            title: 'Exam Date',
            type: 'datetime',
            null_value: '',
            format: 'yyyy-MM-dd',
            sortable: true,
            adjustable: true,
            align: 'center'
        },
        'source': {
            name: 'source',
            title: 'Source',
            type: 'text',
            null_value: '(unknown)',
            format: null,
            sortable: true,
            adjustable: true,
            filter: false,
            align: 'center'
        }
    }

    _get_frame () {
        var frames = {
            frame: function () {
                var output = "";
                output += "<div class='" + this.style_cls + "'>";
                output += "    <div class=frame>";
                output += "        <div class=result_title>搜尋結果</div>";
                output += "        <div class=result_table></div>";
                output += "    </div>";
                output += "</div>";
                return output; 
            }.bind(this)
            //frame1: fucntion (...) {},
            //frame2: fucntion (...) {},
        }
        return $.extend({}, super._get_frame(), frames)
    }

    create () {
        var sel = "." + this.style_cls;
        if (this.jParent.find(sel).length != 0) {
            this.jParent.find(sel).remove();
        }

        var frame = this._get_frame().frame();
        this.jParent.append(frame);

        var jObj = this.jParent.find(sel)
        return jObj;
    }

    set_clickRowManual (func) {
        if (typeof func != 'function')
            throw 'input argument "func" should be "function"';
        this.table.updateSetting({clickRowManual: func});
    }

    set_unclickRowManual (func) {
        if (typeof func != 'function')
            throw 'input argument "func" should be "function"';
        this.table.updateSetting({unclickRowManual: func});
    }

    set_batchOperation (batch_operation_objs) {
        if (typeof func != 'object')
            throw 'input argument "batch_operation_objs" should be "object"';
        this.table.updateSetting(batch_operation_objs);
    }

    setData (data) {
        var format_data = [];
        for (var i in data) {
            var row = data[i];
            format_data.push({
                study_id: row['study_id'],
                patient_id: row['patient_id'],
                body_part: row['body_part'],
                modality: row['modality'],
                source: row['source'],
                exam_date: (row['exam_date'] != null) ? new Date(row['exam_date']) : null
            });
        }
        this.table.setData(format_data);
    }
}
