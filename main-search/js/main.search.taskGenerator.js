class MainSearchTaskGenerator extends ModuleTemplate {
    constructor (jHidden_id, data) {
        super(jHidden_id);
        this.jParent = this.jHidden;
        this.dialog_setting = null;
        this._default_setting = {
            'title': '',
            'width': 400,
            'height': 600,
            'min-width': 330
        };

        this.style_cls = "main-search-task_generator";
        var sel = '.' + this.style_cls;

        this.jHidden.append(this._get_frame().frame());
        this.jObj = this.jHidden.find(sel);
        this.jInfo = this.jObj.find('.generator-info');
        this.jOption = this.jObj.find('.generator-option');
        this.jLabeler = this.jObj.find('.generator-labeler');
        this.jConfirmer = this.jObj.find('.generator-confirmer');

        this._project_id = data.project_id;
        this._project_name = data.project_name;
        this._study_id_list = data.study_id_list;
        this._user_list = data.user_list;

        this._update();
        this.bindEvents();
    }

    bindEvents () {
        this.jObj.off('click', '.checkbox_unchecked')
        this.jObj.on('click', '.checkbox_unchecked',
            this._operate_on_checkbox.to_check.bind(this)
        );
        this.jObj.off('click', '.checkbox_checked')
        this.jObj.on('click', '.checkbox_checked',
            this._operate_on_checkbox.to_uncheck.bind(this)
        );
        this.jObj.off('click', '.generator-submit')
        this.jObj.on('click', '.generator-submit',
            this._operate_on_button.submit.bind(this)
        );
        this.jObj.off('click', '.generator-cancel')
        this.jObj.on('click', '.generator-cancel',
            this._operate_on_button.cancel.bind(this)
        );
    }

    unbindEvents () {
        this.jObj.off();
        this.jObj.unbind();
    }

    destroy () {
        this.unbindEvents();
        this.jObj.remove();
        console.log(this)
    }

    _get_frame () {
        var frames = {
            frame: function () {
                var output = "";
                output += "<div class='" + this.style_cls + "'>";
                output += "    <div class=generator-info></div>";
                //output += "    <div class=generator-option></div>";
                output += "    <div class=generator-labeler>";
                output += "        <div class=title>標註者</div>";
                output += "        <div class=content></div>";
                output += "    </div>";
                output += "    <div class=generator-confirmer>";
                output += "        <div class=title>確認者</div>";
                output += "        <div class=content></div>";
                output += "    </div>";
                output += "    <button class=generator-submit>Submit</button>"
                output += "    <button class=generator-cancel>Cancel</button>"
                output += "</div>";
                return output;
            }.bind(this),

            select_unit: function (uid, title, check=false) {
                var ckcls = (check) ? 'checkbox_checked' : 'checkbox_unchecked';

                var output = "";
                output += "<div class=select_unit uid='" + uid + "'>";
                output += "    <span class='" + ckcls + "'></span>";
                output += "    <span class=title>" + title + "</span>";
                output += "</div>";
                return output;
            },

            info_block: function (project_name, study_id_list) {
                var output = "";
                output += "<p>專案名稱： " + project_name + "</p>";
                output += "<p>選取Study (共" + study_id_list.length + "筆)：<br>";
                for (var i in study_id_list) {
                    if (i != 0)
                        output += " , "
                    output += "<font color='blue'>" + study_id_list[i] +"</font>"
                }
                return output;
            }
        }
        return $.extend({}, super._get_frame(), frames);
    }

    _operate_on_checkbox = {
        to_check: function (e) {
            var tar = $(e.currentTarget);
            tar.addClass('checkbox_checked').removeClass('checkbox_unchecked');
        },
        to_uncheck: function (e) {
            var tar = $(e.currentTarget);
            tar.addClass('checkbox_unchecked').removeClass('checkbox_checked');
        }
    }

    _operate_on_button = {
        submit: function (e) {
            var jlabeler_list = this.jObj.find('.generator-labeler .checkbox_checked');
            var labeler_list = [];
            if (jlabeler_list.length == 0) {
                alert("請至少選擇一位標註者");
                return;
            }

            for (var i=0; i<jlabeler_list.length; i++)
                labeler_list.push($(jlabeler_list[i]).parent().attr('uid'));

            var jconfirmer_list = this.jObj.find('.generator-confirmer .checkbox_checked');
            var confirmer_list = [];
            for (var i=0; i<jconfirmer_list.length; i++)
                confirmer_list.push($(jconfirmer_list[i]).parent().attr('uid'));

            this._api_generate_task(
                this._project_id,
                this._study_id_list,
                labeler_list,
                confirmer_list,
                this.close_dialog.bind(this)
            );
        },
        cancel: function (e) {
            this.close_dialog();
        }
    }

    _api_generate_task (project_id, study_id_list, labeler_list, confirmer_list, func=null) {
        console.log(project_id)
        var req = {
            project_id: project_id,
            study_id_list: JSON.stringify(study_id_list),
            labeler_list: JSON.stringify(labeler_list),
            confirmer_list: JSON.stringify(confirmer_list)
        }
        $.post(ip_port_ui + "generate_multi_confirm_notself", req, function (func, data) {
            var res = JSON.parse(data);

            if (res['status'][0] == 100) {
                var user_list = res['user_list'];
                show_toast('', '新增成功！', 1000);
            } else 
                throw "/generate_multi_confirm_notself : " + res['status'][0] + ' : ' + res['status'][1] + ' : ' + res['status'][2];

            if (func != null && typeof func != 'function')
                throw 'input argument "func" should be "function"';

            if (func != null) func();
        }.bind(this, func));
    }

    _update () {
        this._update_info();
        //this._update_option();
        this._update_labeler();
        this._update_confirmer();
    }

    _update_info () {
        this.jInfo.empty();
        this.jInfo.append(
            this._get_frame().info_block(
                this._project_name,
                this._study_id_list 
            )
        );
    }

    _update_option () {
        this.jOption.empty();
    }

    _update_labeler () {
        var jObj = this.jLabeler.find('.content');
        jObj.empty();
        for (var i in this._user_list) {
            var user = this._user_list[i];
            var uid = user.email1;
            var name = user.name1;
            var privilege = user.privilege;
            jObj.append(
                this._get_frame().select_unit(uid, name, false)
            );
        }
    }

    _update_confirmer () {
        var jObj = this.jConfirmer.find('.content');
        jObj.empty();
        for (var i in this._user_list) {
            var user = this._user_list[i];
            var uid = user.email1;
            var name = user.name1;
            var privilege = user.privilege;
            if (privilege != 'confirmer') continue;
            jObj.append(
                this._get_frame().select_unit(uid, name, false)
            );
        }
    }

    set_parent (jParent) {
        this.jObj.detach().appendTo(jParent);
    }

    detach () {
        this.jObj.detach().appendTo(this.jHidden);
    }

    open_dialog () {
        if (this.dialog_setting == null) {
            super.open_dialog(this._default_setting);
        } else {
            super.open_dialog();
        }
        this.set_parent(this.m_dialog);
    }

    close_dialog () {
        if (this.get_dialog() == null) return;
        this.detach();
        super.close_dialog();
        this.destroy();
    }
}
