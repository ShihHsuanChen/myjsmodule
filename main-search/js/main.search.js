// Search function in the worklist page
// Components:
//  1. main panel: search option, criteria
//  2. result: table
//  3. result pannel: to control the table
//  4. preview (?): when click(?) one line of table, preview the study
// Structure
//  top-class
//      L constructor
//      L destroyer
//      L event listener (bindevent)
//      L event operators
//      L block operatiors (include generators)
//      L member-class
//          L main pannel
//              L constructor
//              L destroyer
//              L event listener (bindevent)
//              L member-class
//          L result
//              L constructor
//              L destroyer
//              L event listener (bindevent)
//              L member-class
//                  L result pannel
//                  L result table
//          L preview
//              L constructor
//              L destroyer
//              L event listener (bindevent)
//              L member-class
//

class MainSearch extends ModuleTemplate {
    constructor (jHidden_id, user) {
        super(jHidden_id);
        this.jParent = this.jHidden;
        this.dialog_setting = null;
        this._default_setting = {
            title: '搜尋',
            width: 1500,
            height: 900
        };

        this.style_cls = "main-search";
        var sel = "." + this.style_cls;
        
        this.jHidden.append(this._get_frame().frame());
        this.jObj = this.jHidden.find(sel);
        this.jPannel = this.jObj.find('.search-pannel');
        this.jResult = this.jObj.find('.search-result');
        this.jPreview = this.jObj.find('.search-preview');

        this.m_pannel = new MainSearchPannel(
            jHidden_id,
            {
                user: user,
                searchSubmit: this._pannelSubmit.bind(this)
            }
        );
        this.m_result = new MainSearchResult(
            jHidden_id,
            {clickRowManual: this._resultClickRow.bind(this),
             batch_operation: this._batch_operation
            }
        );
        this.m_preview = new MainSearchPreview(jHidden_id);

        this.bindEvents();
    }

    bindEvents() {
        this.jObj.ready(function () {
            this.jPannel.resizable({handles: "e"});
            this.jResult.resizable({handles: "e"});
        }.bind(this));

        $('body').off("keyup");
        $('body').on("keyup",
            this._operate_shortcut.keyup.bind(this)
        );
        $('body').on("keydown",
            this._operate_shortcut.keydown.bind(this)
        );
    }

    unbindEvents() {
    }

    destroy () {
        this.m_pannel.destroy();
        this.m_result.destroy();
        this.m_preview.destroy();
        this.unbindEvents();
    }

    _operate_shortcut = {
        keyup: function (e) {
            var input = e.key;
            var keycode = e.keyCode;
            // ESC
            if (e.keyCode === 27)
                if (this.get_dialog() != null)
                    this.m_dialog.dialog('close');
        },
        keydown: function (e) {
            var input = e.key;
            var keycode = e.keyCode;
        }
    }

    _get_frame () {
        var frames = {
            frame: (function () {
                var output = "";
                output += "<div class='" + this.style_cls + "'>"
                output += "    <div class=search-pannel></div>";
                output += "    <div class=search-result></div>";
                output += "    <div class=search-preview></div>";
                output += "</div>";
                return output;
            }).bind(this),

            //frame1: fucntion (...) {},
            //frame2: fucntion (...) {},
        }
        return $.extend({}, super._get_frame(), frames);
    }

    // api
    _api_get_study_info (study_id, func=null) {

        this._loading_dialog = new LoadingDialog('id_loading', 200, 100, '資料載入中', "");

        var req = {study_id: study_id}

        $.post(ip_port_ui + "get_study_info", req, function (func, data) {
            var res = JSON.parse(data);

            if (res['status'][0] == 100) {
                var study_info = res['study_info'];
                var user_info = res['user_info'];
                var project_info = res['project_info'];
            } else 
                throw "/search_study : " + res['status'][0] + ' : ' + res['status'][1] + ' : ' + res['status'][2];

            if (func != null && typeof func != 'function')
                throw 'input argument "func" should be "function"';

            var info = {
                study_info: study_info,
                user_info: user_info,
                project_info: project_info
            }
            if (func != null) func(info)
        }.bind(this, func)).done(function () {
            this._loading_dialog.close();
            this._loading_dialog = null;
        }.bind(this));
    }

    _api_get_project_userlist (project_id, func=null) {
        var req = {project_id: project_id};

        $.post(ip_port_ui + "get_project_userlist", req, function (func, data) {
            var res = JSON.parse(data);

            if (res['status'][0] == 100) {
                var user_list = res['user_list'];
            } else 
                throw "/get_project_userlist : " + res['status'][0] + ' : ' + res['status'][1] + ' : ' + res['status'][2];

            if (func != null && typeof func != 'function')
                throw 'input argument "func" should be "function"';

            if (func != null) func(user_list);
        }.bind(this, func));
    }

    /* linking of member event listener functions */
    _pannelSubmit (criterian, data) {
        this._criterian = criterian;
        this.m_result.setData(data);
    }

    _resultClickRow (data) {
        var study_id = data.study_id;
        this._api_get_study_info(study_id, function (info) {
            var study_info = info.study_info;
            var project_info = info.project_info;
            var user_info = info.user_info;

            var project_name_list = [];
            for (var project_id of study_info.project_id_list) {
                var pid = '' + project_id;
                project_name_list.push(project_info[pid].project_name)
            }

            //var task_objs = {};
            var task_list = [];
            for (var task of study_info.task_list) {
                var project_id = '' + task['project_id'];
                var labeler_acc = task['labeler_email'];
                var confirmer_acc = task['confirmer_email'];
                var project_name = project_info[project_id].project_name;

                var title = 'No.' + task.task_id + '(' + project_name + ')';
                var label_list = task.label_list;
                //var label_list = [];
                var new_task = {
                    'Task ID': task.task_id,
                    '所屬專案': project_name,
                    '標註者': (labeler_acc != null) ? user_info[labeler_acc].name : "",
                    '確認者': (confirmer_acc != null) ? user_info[confirmer_acc].name : "",
                    '標註狀態': (task.label_date != null) ? '已標註' : '未標註',
                    '標註數目': task.label_list.length,
                    '標註日期': (task.label_date != null) ? task.label_date : "",
                    '標註清單': label_list
                };
                //task_objs[title] = new_task;
                task_list.push(new_task);
            }
            var show_data = [
                {group: 'study', title: '檢查序號 (Study ID)' , value: study_info.study_id},
                {group: 'study', title: '病患ID (Patient ID)' , value: study_info.patient_id},
                {group: 'study', title: '儀器 (Modality)'     , value: study_info.modality},
                {group: 'study', title: '部位 (Part)'         , value: study_info.body_part},
                {group: 'study', title: '檢查日期 (Exam Date)', value: study_info.exam_date },
                {group: 'study', title: '資料來源 (Source)'   , value: study_info.source},
                {group: 'study', title: '包含的Series'        , value: study_info.series_description_list},
                {group: 'task', title: '使用專案'            , value: project_name_list},
                {group: 'task' , title: 'Tasks'               , value: task_list}
                //{group: 'task' , title: 'Tasks'               , value: task_objs}
            ]
            this.m_preview.showData(show_data);
        }.bind(this))
    }

    /* */
    _batch_operation = {
        'add': {
            key: 'add',
            html: "<div class=icon-add-task title='新增標註者'></div>",
            func: function (data) {
                var study_id_list = [];
                for (var i in data)
                    study_id_list.push(data[i].study_id);
                this.add_task(study_id_list);
            }.bind(this)
        },
    }

    add_task (study_id_list) {
        var project_id = this._criterian.project_id;
        this._api_get_project_userlist(
            project_id,
            this.open_generator_dialog.bind(this, project_id, study_id_list)
        );
    }

    open_generator_dialog (project_id, study_id_list, user_list) {
        // fake data
        var data = {
            project_id: 34,
            project_name: 'cxr dicom',
            study_id_list: study_id_list,
            user_list: user_list
        }
        var gen_dialog = new MainSearchTaskGenerator(this.jHidden.attr('id'), data);
        gen_dialog.open_dialog()
    }

    set_parent (jParent) {
        this.jObj.detach().appendTo(jParent);
    }

    set_parent_pannel (jParent) {
        this.m_pannel.set_parent(jParent);
    }

    set_parent_result (jParent) {
        this.m_result.set_parent(jParent);
    }

    set_parent_preview (jParent) {
        this.m_preview.set_parent(jParent);
    }

    get_parent () {
    }

    get_parent_pannel () {
        return this.m_pannel.get_parent();
    }

    get_parent_result () {
        return this.m_result.get_parent();
    }

    get_parent_preview () {
        return this.m_preview.get_parent();
    }

    detach () {
        this.jObj.detach().appendTo(this.jHidden);
    }

    detach_pannel () {
        this.m_pannel.detach();
    }

    detach_result () {
        this.m_result.detach();
    }

    detach_preview () {
        this.m_preview.detach();
    }

    open_dialog () {
        if (this.dialog_setting == null) {
            super.open_dialog(this._default_setting);
        } else {
            super.open_dialog();
        }
        if (this.m_pannel.jHidden === this.m_pannel.get_parent())
            this.set_parent_pannel(this.jPannel);
        if (this.m_result.jHidden === this.m_result.get_parent())
            this.set_parent_result(this.jResult);
        if (this.m_preview.jHidden === this.m_preview.get_parent())
            this.set_parent_preview(this.jPreview);

        this.set_parent(this.m_dialog);
    }

    close_dialog () {
        if (this.get_dialog() == null) return;
        this.detach();
        super.close_dialog();
    }
}
