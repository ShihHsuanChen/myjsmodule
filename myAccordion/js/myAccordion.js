function myAccordionActivate (jObj) {
    // jObj: .myaccordion object
    jObj.find('div.myaccordion-content:first').hide();
    jObj.on('click', 'div.myaccordion-title:first',  function() {
        $t = $(this);
        $c = $($t.parent().children()[1]);
        $c.slideUp();
        $t.removeClass("open");
        if($c.css("display")=="none"){
            $c.slideDown();
            $t.addClass("open");
        }
    }).mouseover(function(){
        $(this).addClass("rollover")
    }).mouseout(function(){
        $(this).removeClass("rollover")
    });
}

function myAccordionUnit (list_data) {
    var output = "";
    output += "<ul class=myaccordion>"
    for (var i in list_data) {
        output += "<li>";
        output += "    <div class=myaccordion-title>" + i + "</div>";
        output += "    <div class=myaccordion-content>";
        output += list_data[i];
        output += "    </div>";
        output += "</li>";
    }
    output += "</ul>"
    return output;
}
