class LoadingDialog {
    constructor (id, width, height, text, text_style, destroy_func) {
        this.base_path = this._get_base_path();

        var content = this._get_content(text, text_style);

        this.jObj = this._loading_dialog(id, width, height, content, destroy_func);
        this.open();
    }

    _destroy () {
        this._unbindEvents();
        this.jObj.remove();
        this.jObj.parent().remove();
        this.jObj = null;
    }

    _bindEvents () {
    } 

    _unbindEvents () {
        this.jObj.off();
        this.jObj.unbind();
    }

    _get_content (text, text_style) {
        this.base_path
        var cssfile = this.base_path + "myloading.css";
        var output = "";
        output += "<link rel='stylesheet' href='" + cssfile + "'>";
        output += "<div style='position:inherit'>";
        //output += "<div style='position:absolute;left:0px;right:0px;top:0px;bottom:0px;'>";
        if (text_style == null)
            output += "    <div class=loader_text>";
        else
            output += "    <div class=loader_text style='" + text_style + "'>";
        output += text;
        output += "    </div>";
        output += "    <div class=loader12></div>";
        output += "</div>";
        return output;
    }

    _url (suburl) {
        if (this.base_path==null)
            this.base_path = this._get_base_path();
        return this.base_path + suburl;
    }

    _get_base_path () {
        var scripts = document.getElementsByTagName('script');
        var base_path = null;

        var re = /.+(?=\/myloading\.js$)/;
        for (var i in scripts) {
            var src = scripts[i].src;
            var res = src.match(re);
            if (res!=null) {
                base_path = res[0] + "/";
                break;
            }
        }
        return base_path;
    }

    _loading_dialog(id, width, height, content, destroy_func) {
        $.ui.dialog.prototype._focusTabbable = function(){};
        var b_dialog = $("<div id='" + id + "'></div>")
            .html(content)
            .dialog({
                autoOpen: false,
                draggable: false,
                resizable: false,
                modal: true,
                height: height,
                width: width,
                close: function (event, ui) {
                    $(this).dialog('destroy').remove();
                    if (destroy_func)
                        destroy_func();
                },
                closeOnEscape: false
            });
        b_dialog.parent().find(".ui-dialog-titlebar").hide();
        return b_dialog;
    }

    getDialog () {
        return this.jObj.parent();
    }

    open () {
        this.jObj.dialog('open');
    }

    close () {
        this.jObj.dialog("close");
        this._destroy();
    }
}

